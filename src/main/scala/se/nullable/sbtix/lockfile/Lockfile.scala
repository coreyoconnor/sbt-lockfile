package se.nullable.sbtix.lockfile

import io.circe._
import io.circe.generic.JsonCodec
import java.nio.file.{Path, Paths}

import PathCodec._

@JsonCodec
case class Lockfile(files: Map[Path, LockfileEntry])

@JsonCodec
case class LockfileEntry(url: String, sha256: String)

object PathCodec {
  implicit val pathDecoder: Decoder[Path] =
    Decoder.decodeString.map(Paths.get(_))
  implicit val pathEncoder: Encoder[Path] =
    Encoder.encodeString.contramap(_.toString())

  implicit val pathKeyDecoder: KeyDecoder[Path] =
    KeyDecoder.decodeKeyString.map(Paths.get(_))
  implicit val pathKeyEncoder: KeyEncoder[Path] =
    KeyEncoder.encodeKeyString.contramap(_.toString())
}
