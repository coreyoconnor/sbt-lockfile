# sbt-lockfile

This is an attempt to rewrite the SBT plugin part of [Sbtix](https://gitlab.com/teozkr/Sbtix) to be independent
of Nix. For now, see [teozkr/Sbtix#41](https://gitlab.com/teozkr/Sbtix/issues/41) for more details.

## Usage

Add the plugin to your `project/plugins.sbt`:

```scala
resolvers += Resolver.sonatypeRepo("snapshots")
addSbtPlugin("se.nullable.sbtix" %% "sbt-lockfile" % "0.1-SNAPSHOT")

enablePlugins(LockfilePlugin)
```

Now it's possible to generate a `sbt.lock` which contains all of your build artifacts
and `sha256` checksums:

```
$ sbt freeze
$ sbt lockfileRequireFrozen
```

Whenever you regenerate the lockfile it will verify that all hashes match and adds newly
added artifacts.

## Development

## Testing

Functional tests use `scripted-plugin`:

```
$ sbt scripted
```

## Continuous Deployment

Each commit on `master` to `teozkr/sbt-lockfile` is published to Sonotype as a SNAPSHOT automatically.

## Testing Local changes

To test local changes to `sbt-lockfile` with other sbt projects use `publishLocal`:

```
$ sbt publishLocal
```

To revert, `rm ~/.ivy2/local/se.nullable.sbtix/sbt-lockfile`.

## Contact

Do you have any questions about this library or want to help out?

Feel free to [open an issue](https://gitlab.com/teozkr/sbt-lockfile/issues/new)
or join  `#sbtix` on freenode.
